package com.atlassian.stash.plugin.inbox.rest;

import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.rest.exception.UnhandledExceptionMapper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class InboxServiceExceptionMapper extends UnhandledExceptionMapper {
    public InboxServiceExceptionMapper(NavBuilder navBuilder) {
        super(navBuilder);
    }
}
