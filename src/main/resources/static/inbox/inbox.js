define('plugin/inbox/inbox', [
    'jquery',
    'aui',
    'util/events',
    'util/navbuilder',
    'util/ajax',
    'model/page-state',
    'feature/pull-request/pull-request-table',
    'widget/avatar-list',
    'exports'
], function (
    $,
    AJS,
    events,
    navBuilder,
    ajax,
    pageState,
    PullRequestsTable,
    AvatarList,
    exports
) {
    var pullRequestTable,
        dialogInitialised = false,
        inlineDialog,
        $inboxTrigger;

    function getInboxResourceUrlBuilder() {
        return navBuilder.newBuilder().pushComponents('rest', 'inbox', 'latest', 'pull-requests').makeBuilder();
    }

    function getInboxCountResourceUrl() {
        return navBuilder.newBuilder().pushComponents('rest', 'inbox', 'latest', 'pull-requests', 'count').makeBuilder().build();
    }

    var hideOnEscapeKeyUp = function(e) {
        if(e.keyCode === $.ui.keyCode.ESCAPE) {
            inlineDialog.hide();
            e.preventDefault();
        }
    };

    var handleInboxError = function($content, response) {
        var defaultError = {
            title: stash_i18n('stash.plugin.inbox.error.title', 'Could not retrieve inbox'),
            message: stash_i18n('stash.plugin.inbox.error.unknown', 'An unknown error occurred')
        };

        var responseError = {};
        if (response) {
            responseError = response.errors ?
                response.errors[0] :
                response;
        }
        var error = $.extend({}, defaultError, responseError);

        $content.html($(stash.widget.dialogMessage.error({
            title: error.title,
            text: error.message,
            extraClasses: 'communication-error'
        })));
        return false;
    };

    var onShowDialog = function ($content, trigger, showPopup) {
        if (!dialogInitialised) {
            dialogInitialised = true;

            var $dialogContents = $(stash.plugin.inbox.dialogContents());

            $content.append($dialogContents);

            events.once('stash.plugin.inbox.dataLoaded', function () {
                $('#inbox-pull-request-table').removeClass("hidden");
            });

            var handleError = function(xhr, textStatus, errorThrown, resp) {
                return handleInboxError.call(this, $content, resp);
            };

            pullRequestTable = new PullRequestsTable('open', null, getInboxResourceUrlBuilder, {
                'scope': 'global',
                'target': '#inbox-pull-request-table',
                'scrollPaneSelector': '.inbox-table-wrapper',
                'bufferPixels': 50,
                'pageSize': 10,
                'spinnerSize': 'medium',
                'noneFoundMessageHtml': stash.plugin.inbox.emptyInboxMessage(),
                'dataLoadedEvent': 'stash.plugin.inbox.dataLoaded',
                'statusCode': {
                    0: handleError,
                    401: handleError,
                    500: handleError,
                    502: handleError
                }
            });
            pullRequestTable.handleErrors = $.noop; // we handle the errors ourselves
            pullRequestTable.init();
            AvatarList.init();
        }
        showPopup();

        $(document).on('keyup', hideOnEscapeKeyUp);
    };

    var onHideDialog = function () {
        $(document).off('keyup', hideOnEscapeKeyUp);
        if ($(document.activeElement).closest('#inbox-pull-requests-content').length) {
            // if the focus is inside the dialog, you get stuck when it closes.
            document.activeElement.blur();
        }
    };

    var fetchInboxCount = function() {
        ajax.rest({
            url: getInboxCountResourceUrl(),
            type: 'GET',
            statusCode: {
                '*': function () {
                    return false;
                }
            }
        }).done(function (data) {
            if (data.count > 0) {
                var $badge = $(aui.badges.badge({
                    'text': data.count
                }));
                $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: false}))
                    .append($badge);
                setTimeout(function () {
                    $badge.addClass('visible');
                }, 200);
            } else {
                // The badge fadeOut transition happens with a CSS3 transition, which we can't hook into.
                // Use a setTimeout instead, unfortunately.
                var cssTransitionDuration = 500;
                $inboxTrigger.find(".aui-badge").removeClass('visible');
                setTimeout(function () {
                    $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: true}));
                }, cssTransitionDuration);
            }
        });
    };

    exports.onReady = function () {
        $inboxTrigger = $("#inbox-pull-requests");
        if ($inboxTrigger.length && pageState.getCurrentUser()) {
            $inboxTrigger.html(stash.plugin.inbox.triggerIcon({isEmpty: true}));
            inlineDialog = AJS.InlineDialog($inboxTrigger, 'inbox-pull-requests-content', onShowDialog, {
                width: 840,
                hideCallback: onHideDialog
            });

            fetchInboxCount();

            var _approvalHandler = function(data) {
                if (data.user.name === pageState.getCurrentUser().name) {
                    fetchInboxCount();
                }
            };

            events.on('stash.widget.approve-button.added', _approvalHandler);
            events.on('stash.widget.approve-button.removed', _approvalHandler);
        }
    };
});

AJS.$(document).ready(function () {
    require('plugin/inbox/inbox').onReady();
});
